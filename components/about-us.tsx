import { useEffect, useRef } from "react"
import Image from 'next/image'
import SectionHeader from "./section/section-header"

interface Props {
    onIntersecting: Function
}
const AboutUsSection: React.FC<Props> = ({ onIntersecting }) => {
    const mainRef = useRef<HTMLDivElement>(null)

    useEffect(() => {
        const { current: element } = mainRef
        if (element) onIntersecting(element)
    }, [mainRef, onIntersecting])



    return <>
        <section className="lg:mx-12 mx-4 my-4 snap-start">
            <SectionHeader title="Tentang Desa" ref={mainRef}>Tentang Desa</SectionHeader>
            <div className="flex flex-col gap-y-8 lg:gap-x-8 lg:gap-y-0 lg:flex-row mt-8 ">
                <div className="lg:flex-1 lg:h-auto h-64 relative">
                    <div className="w-11/12 h-5/6 absolute top-0 left-0 rounded-lg shadow-2xl overflow-hidden border-gray-100 border-2">
                        <Image src="/assets/about-bg.jpg" layout="fill" objectFit="cover" alt="about image 1"/>
                    </div>
                    <div className="w-11/12 h-5/6 absolute bottom-0 right-0 rounded-lg shadow-2xl overflow-hidden border-gray-100 border-2">
                        <Image src="/assets/about-bg.jpg" layout="fill" objectFit="cover" alt="about image 2"/>
                    </div>
                </div>

                <div className="flex-1 bg-white rounded-lg shadow-xl text-justify text-xs lg:text-xl lg:p-8 p-4 flex flex-col lg:gap-y-8 gap-y-2">
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit, sapiente dicta incidunt doloribus ex soluta fugiat saepe odit facere provident vitae, nam eaque harum est animi natus modi unde ullam?
                    Neque vitae inventore ea, reprehenderit enim deserunt natus, iure maiores nisi numquam ipsa quae vero aut, eveniet quasi praesentium dicta! Quae, veniam itaque consectetur totam consequatur nisi expedita a dolore.</p>

                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis, sit. Repudiandae, odio itaque numquam doloremque ratione ea vitae aperiam soluta est ab suscipit maiores unde, obcaecati impedit veniam quo perspiciatis!
                    Reprehenderit deleniti voluptate nostrum neque! Recusandae doloremque inventore vel eveniet ad, eum et illum assumenda accusamus nihil sequi molestiae amet temporibus numquam ratione, laudantium, omnis sed praesentium tempora suscipit voluptates!
                    Minima itaque, neque vero eius, libero praesentium aliquid voluptatum quis, dignissimos sed ex cumque debitis quibusdam deleniti quisquam! Repellendus quam maiores ad non sunt obcaecati omnis sed accusamus temporibus quis.</p>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eum dolor quasi nemo esse quidem deleniti aliquam excepturi, ex iusto, repellendus, cumque temporibus! Voluptas corrupti quas, molestiae tenetur laboriosam eos facilis?
                    Minima sit commodi voluptatibus neque veritatis, earum ducimus aliquid alias eum debitis dolorem optio eaque pariatur, corrupti temporibus vitae libero exercitationem fugit dolor perspiciatis! Maxime quibusdam esse veritatis magni eos!</p>
                </div>

            </div>
        </section>
    </>
}

export default AboutUsSection
