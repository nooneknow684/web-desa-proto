
import { useEffect, useRef, useState, useCallback } from "react"
import { SectionProp } from "./section"
import { Transition, Tab } from '@headlessui/react'
import SectionHeader from "./section/section-header"
import React from "react"

const history = [
    {
        year: 1992,
        text: `<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolorem eius dolor inventore recusandae deserunt eaque, cum nisi, quam, magni dolores modi blanditiis voluptas mollitia est beatae quidem nemo ullam. Et.</p>
                    <p>Quasi impedit veritatis blanditiis beatae atque repellat sint magnam similique dignissimos perspiciatis qui a soluta obcaecati quia, totam corrupti! Atque non deleniti nulla itaque sapiente, corrupti nostrum recusandae nobis voluptatibus?</p>
                    <p>Mollitia officiis eligendi repellat minus id nesciunt officia quidem, harum cumque maiores quae repellendus aut quo eveniet alias? Suscipit saepe, excepturi porro modi minus quis consequatur fugit molestiae odit in.</p>
                    <p>Fugit voluptatibus corporis fuga nostrum dolorem in est dolore! A ullam esse vitae placeat, velit accusamus? Quo quas ipsum praesentium hic. Deserunt, aspernatur doloremque. Aspernatur nulla quasi facere magni explicabo.</p>
                    <p>Iusto, consequuntur debitis! Vel, doloremque veniam? Blanditiis dolorem excepturi dolorum. Ipsa magnam velit qui cumque. Blanditiis ullam voluptatem dolore accusantium consequuntur consequatur a corporis, illum culpa dolor iure ad saepe?</p>`
    }, {
        year: 1995,
        text: `<p>Fugit voluptatibus corporis fuga nostrum dolorem in est dolore! A ullam esse vitae placeat, velit accusamus? Quo quas ipsum praesentium hic. Deserunt, aspernatur doloremque. Aspernatur nulla quasi facere magni explicabo.</p>
                    <p>Iusto, consequuntur debitis! Vel, doloremque veniam? Blanditiis dolorem excepturi dolorum. Ipsa magnam velit qui cumque. Blanditiis ullam voluptatem dolore accusantium consequuntur consequatur a corporis, illum culpa dolor iure ad saepe?</p>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolorem eius dolor inventore recusandae deserunt eaque, cum nisi, quam, magni dolores modi blanditiis voluptas mollitia est beatae quidem nemo ullam. Et.</p>
                    <p>Quasi impedit veritatis blanditiis beatae atque repellat sint magnam similique dignissimos perspiciatis qui a soluta obcaecati quia, totam corrupti! Atque non deleniti nulla itaque sapiente, corrupti nostrum recusandae nobis voluptatibus?</p>
                    <p>Mollitia officiis eligendi repellat minus id nesciunt officia quidem, harum cumque maiores quae repellendus aut quo eveniet alias? Suscipit saepe, excepturi porro modi minus quis consequatur fugit molestiae odit in.</p>
                    <p>Fugit voluptatibus corporis fuga nostrum dolorem in est dolore! A ullam esse vitae placeat, velit accusamus? Quo quas ipsum praesentium hic. Deserunt, aspernatur doloremque. Aspernatur nulla quasi facere magni explicabo.</p>
                    <p>Iusto, consequuntur debitis! Vel, doloremque veniam? Blanditiis dolorem excepturi dolorum. Ipsa magnam velit qui cumque. Blanditiis ullam voluptatem dolore accusantium consequuntur consequatur a corporis, illum culpa dolor iure ad saepe?</p>`
    }
]

const HistorySection: React.FC<SectionProp> = ({ onIntersecting }) => {
    const mainRef = useRef<HTMLDivElement>(null)

    useEffect(() => {
        const { current: element } = mainRef
        if (element) onIntersecting(element)
    }, [mainRef, onIntersecting])

    return (
        <section className="text-justify text-xs lg:text-xl px-4 lg:px-8 py-12 ">
            <SectionHeader title="Sejarah Desa" ref={mainRef}>Sejarah Desa</SectionHeader>
            <Tab.Group>
                <Tab.List className="grid justify-items-center place-items-center font-bold text-green-800 p-4 shadow-xl rounded-2xl bg-white mt-8" style={{ gridTemplateColumns: `repeat(${history.length},minmax(0,1fr)` }}>
                    <div className="w-full h-px col-start-1 col-span-full row-start-1 row-end-2 bg-green-800"></div>
                    {history.map((e, index) => (
                        <Tab key={`history-year-${e.year}`} as={React.Fragment}>
                            {({ selected }) => (
                                <button
                                    className={`rounded-xl px-4 row-start-1 row-end-2 ${selected ? "text-white bg-green-800" : "text-green-800 bg-white"}`}
                                    style={{ gridColumnStart: index + 1, gridColumnEnd: index + 2 }}
                                >
                                    {e.year}
                                </button>
                            )}
                        </Tab>
                    ))}
                </Tab.List>
                <Tab.Panels>
                    {history.map((el) => (
                        <Tab.Panel key={`history-text-${el.year}`} className="p-4 lg:p-8 shadow-xl rounded-2xl mt-4 bg-white flex flex-col lg:gap-y-8 gap-y-4" dangerouslySetInnerHTML={{ __html: el.text }} />
                    ))}
                </Tab.Panels>
            </Tab.Group>
        </section>
    )
}

export default HistorySection
