import { MapContainer, TileLayer, Marker } from "react-leaflet"
import "leaflet/dist/leaflet.css"
import { renderToStaticMarkup } from 'react-dom/server'
import React, { useCallback, useEffect, useMemo, useState } from "react"
import L from "leaflet"

import { RadioGroup } from "@headlessui/react"
import GovermentIcon from "./icons/gov-icon"
import MosqueIcon from "./icons/mosque-icon"
import OfficeIcon from "./icons/office-icon"

const center = { lat: -5.4635307, lng: 104.9816494 }
const zoom_level = 15

const places = [
    { id: 'masjid-desa', label: 'Masjid Baiturrahman', pos: { lat: -5.465992467644723, lng: 104.98416536582874 }, type: 'mosque' },
    { id: 'kantor-desa', label: 'Kantor Kepala Desa', pos: { lat: -5.465330304123644, lng: 104.98367720380384 }, type: 'gov' },
    { id: 'sekret-kartar', label: 'Sekreteriat Karang Taruna', pos: { lat: -5.464940481708347, lng: 104.98378449216096 }, type: 'office' },
]

const genIcon = (icon:string) => {
    switch (icon) {
        case 'mosque':
            return <MosqueIcon />
        case 'office':
            return <OfficeIcon />
        case 'gov':
            return <GovermentIcon />
        default:
            break;
    }
}

// const iconMap = new Map<string, React.ReactNode>([
//     ['mosque', <MosqueIcon />],
//     ['office', <OfficeIcon />],
//     ['gov', <GovermentIcon />],
// ])

const MapControl = ({ map }: { map: L.Map }) => {
    const [selectedPlace, setSelectedPlace] = useState(0)
    useEffect(() => {
        map.flyTo(places[selectedPlace].pos, 17)

    }, [selectedPlace, map])

    return <div className="bg-white shadow-2xl rounded-xl order-1 lg:order-none overflow-hidden">
        <RadioGroup value={selectedPlace} onChange={setSelectedPlace}>
            {
                places.map((place, index) => (
                    <RadioGroup.Option key={place.id} value={index} as={React.Fragment}>
                        {({ active, checked }) => (
                            <div
                                className={`transition-colors border-white border font-bold shadow-sm py-2 px-4 ${(checked) ? 'bg-white text-green-800' : 'bg-green-800 text-white hover:bg-green-600 active:bg-green-400'}`}>
                                {place.label}
                            </div>
                        )}
                    </RadioGroup.Option>
                ))
            }
        </RadioGroup>

    </div>

}

const MapDisplay = () => {
    const [map, setMap] = useState<L.Map | null>(null)
    const createIcon = useCallback((type: string) => {
        const icon = genIcon(type)
        return L.divIcon({
            className: "bg-transparent",
            iconSize: [32, 32],
            html: renderToStaticMarkup(<div className="h-8 w-8 fill-green-800">{icon}</div>)
        })
    }, [])

    useEffect(() => {
        if (map) {
            map.setMaxBounds(
                map.getBounds()
            )
        }
    }, [map])

    const displayMap = useMemo(() => (
        <MapContainer
            ref={setMap}
            center={center}
            zoom={zoom_level}
            minZoom={zoom_level}
            maxZoom={zoom_level}
            scrollWheelZoom={false}
            zoomControl={false}
            className="rounded-2xl overflow-hidden shadow-2xl relative z-0">
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {
                places.map(({ pos, id, type }) => (
                    <Marker key={`marker-${id}`} position={pos} icon={createIcon(type)} />
                ))
            }
        </MapContainer>
    ), [createIcon])
    return <div className="mt-8 grid lg:grid-cols-2 lg:grid-rows-1 grid-rows-2 lg:h-[50vh] h-screen lg:gap-x-4 gap-y-4 z-0">
        {map ? <MapControl map={map} /> : null}
        {displayMap}
    </div>
}

export default MapDisplay
