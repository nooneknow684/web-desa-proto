import React, { ForwardedRef } from 'react'
interface Prop { title: string, children: React.ReactNode }
const SectionComponent = ({ children, title }: Prop, ref: ForwardedRef<HTMLDivElement>) => {
    return (
        <div ref={ref} className="grid place-items-center justify-items-center" data-title={title}>
            <div className="h-0.5 w-full col-start-1 col-end-2 row-start-1 row-end-2 bg-green-800/20"></div>
            <h3 className="col-start-1 col-end-2 row-start-1 row-end-2 text-center bg-gray-100 px-2 text-2xl font-bold text-green-800">
                {children}
            </h3>
        </div>
    )
}
const SectionHeader = React.forwardRef<HTMLDivElement, Prop>(SectionComponent)
export default SectionHeader
