import dynamic from "next/dynamic";
import { useEffect, useRef } from "react";
import { SectionProp } from "./section";
import SectionHeader from "./section/section-header";

const LazyMap = dynamic(() => import('./lazy-map'), { ssr: false })

const MapSection: React.FC<SectionProp> = ({ onIntersecting }) => {
    const mainRef = useRef<HTMLDivElement>(null)
    useEffect(() => {
        if (mainRef.current) onIntersecting(mainRef.current)
    }, [mainRef, onIntersecting])
    return <section className="lg:mx-12 mx-4 my-4">
        <SectionHeader title="Peta Desa" ref={mainRef}>Peta Desa</SectionHeader>
        <LazyMap />
    </section>
}
export default MapSection
