import type { NextPage } from 'next'
import Image from 'next/image'
import React, { useCallback, useEffect, useRef, useState } from 'react'
import AboutUsSection from '../components/about-us'
import HistorySection from '../components/history'
import MapSection from '../components/map'

const NavbarItem: React.FC<{ children: React.ReactNode, width?: string }> = ({ children, width = "w-32" }) => {
    return (
        <div className={`relative text-center lg:p-0 p-1 ${width} before:absolute before:block before:content-[''] before before:bottom-0 before:w-full before:bg-white before:h-0.5 before:transition before:scale-x-0 hover:before:scale-x-100`}>
            {children}
        </div>
    )
}

const useSectionObserver = (callback: IntersectionObserverCallback) => {
    const sectionObserver = useRef<IntersectionObserver | null>(null)

    useEffect(() => {
        const { current: intersectionObserver } = sectionObserver
        if (!intersectionObserver) sectionObserver.current = new IntersectionObserver(callback, { threshold: 0, rootMargin: "0px 0px -50% 0px" })
    }, [callback])

    const attachSection = useCallback((element: HTMLElement) => {
        const { current: intersectionObserver } = sectionObserver
        if (intersectionObserver) intersectionObserver.observe(element)
    }, [sectionObserver])

    return { attachSection }
}

const Header: React.FC<{ currentSection: string }> = ({ currentSection }) => {
    const headerContentRef = useRef<HTMLDivElement>(null)

    const [headerOnTop, setHeaderOnTop] = useState(false)

    const intersectionHandler = useCallback<IntersectionObserverCallback>(
        (entries) => {
            const el = entries[0]
            setHeaderOnTop(el.isIntersecting)
        },
        [],
    )

    useEffect(() => {
        const { current: headerContentElement } = headerContentRef
        const observer = new IntersectionObserver(intersectionHandler, { rootMargin: "0px 0px -100% 0px", threshold: 0 })
        if (headerContentElement) {
            observer.observe(headerContentElement)
        }
        return () => {
            observer.disconnect()
        }
    }, [headerContentRef, intersectionHandler])

    return (
        <div className="flex sticky top-0 h-10 z-10" ref={headerContentRef}>
            <div className={`flex text-white font-bold text-lg shadow-2xl w-full transition-colors duration-500 ${headerOnTop ? 'bg-gray-100' : 'bg-green-800'} overflow-hidden`}>
                <div className="bg-green-800 pr-8 pl-4 xl:flex hidden items-center relative">
                    <h1 className={` text-lg font-bold tracking-widest transition duration-500 origin-left ${headerOnTop ? 'scale-100 opacity-100' : 'opacity-0 scale-105'}`} style={{ fontFamily: "Lobster,cursive" }}>Desa Dengklek Sari</h1>
                    <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" className='absolute left-full h-10 w-10'>
                        <path className='fill-green-800' d="M 0,0 L 100,0 L 0,100 Z" />
                    </svg>
                </div>
                <div className={`text-xl text-green-800 font-bold px-20 leading-10 transition-colors duration-500 ${headerOnTop ? "opacity-100" : "opacity-0"}`}>
                    {currentSection}
                </div>
                <div className="flex ml-auto gap-x-5 bg-green-800 px-4 items-center relative ">
                    <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" className='absolute right-full h-10 w-10 drop-shadow-2xl'>
                        <path className='fill-green-800' d="M 0,0 L 100,0 L 100,100 Z" />
                    </svg>
                    <button className="h-10 w-10 transition-colors bg-green-800 stroke-white hover:bg-green-400 lg:hidden ">
                        <svg viewBox='0 0 100 100' xmlns="http://www.w3.org/2000/svg" className=''>
                            <path d="M 10,26 L 90,26 Z" strokeWidth={8} />
                            <path d="M 10,46 L 90,46 Z" strokeWidth={8} />
                            <path d="M 10,66 L 90,66 Z" strokeWidth={8} />
                        </svg>
                    </button>
                    <div className="flex flex-col gap-x-4 lg:flex-row bg-green-800 absolute right-0 top-full min-w-max translate-x-full lg:translate-x-0 lg:static ">
                        <NavbarItem width='min-w-fit lg:w-32'>Tentang Desa</NavbarItem>
                    </div>
                </div>
            </div>
        </div>
    )
}

const Home: NextPage = () => {

    const { attachSection } = useSectionObserver((entries) => {
        entries.forEach(el => {
            if (el.isIntersecting) {
                setCurrentSection(el.target.getAttribute('data-title') ?? '')
            }
        })
    })

    const [currentSection, setCurrentSection] = useState("")

    return (
        <div className='relative flex flex-col bg-gray-100 max-w-full' >
            <div className='h-screen relative grid' >
                <Image src="/bg.jpg" layout='fill' objectFit='cover' objectPosition="top" className="col-start-1 col-end-2 row-start-1 row-end-2" alt="main background"/>
                <div className="bg-black/50 flex flex-col justify-center items-center text-white col-start-1 col-end-2 row-start-1 row-end-2 z-10">
                    <h1 className="text-6xl font-bold tracking-widest" style={{ fontFamily: "Lobster,cursive" }}>Desa Dengklek Sari</h1>
                    <h2 className="text-2xl font-normal mt-10">Ini cuman motto</h2>
                </div>
            </div>
            <Header currentSection={currentSection} />
            <div>
                <AboutUsSection onIntersecting={attachSection} />
                <HistorySection onIntersecting={attachSection} />
                <MapSection onIntersecting={attachSection} />
            </div>
        </div>

    )
}

export default Home
